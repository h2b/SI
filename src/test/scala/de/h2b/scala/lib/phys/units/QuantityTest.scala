/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.math._
import de.h2b.scala.lib.phys.units.base._
import de.h2b.scala.lib.phys.units.derived._
import de.h2b.scala.lib.phys.units.Quantity._

@RunWith(classOf[JUnitRunner])
class QuantityTest extends FunSuite {

  test("basic instances") {
    assert(Quantity(1, one).isInstanceOf[Quantity[NeutralUnit]])
    assert(Quantity(1, metre).isInstanceOf[Quantity[LengthUnit]])
    assert(Quantity(1, kilogram).isInstanceOf[Quantity[MassUnit]])
    assert(Quantity(1, second).isInstanceOf[Quantity[TimeUnit]])
    assert(Quantity(1, ampere).isInstanceOf[Quantity[ElectricCurrentUnit]])
    assert(Quantity(1, kelvin).isInstanceOf[Quantity[ThermodynamicTemperatureUnit]])
    assert(Quantity(1, mole).isInstanceOf[Quantity[AmountOfSubstanceUnit]])
    assert(Quantity(1, candela).isInstanceOf[Quantity[LuminousIntensityUnit]])
    assert(Quantity(1, metrePerSecond).isInstanceOf[Quantity[VelocityUnit]])
    assert(Quantity(1, metrePerSecondSquared).isInstanceOf[Quantity[AccelerationUnit]])
    assert(Quantity(1, hertz).isInstanceOf[Quantity[FrequencyUnit]])
    assert(Quantity(1, newton).isInstanceOf[Quantity[ForceUnit]])
    assert(Quantity(1, pascal).isInstanceOf[Quantity[PressureUnit]])
    assert(Quantity(1, joule).isInstanceOf[Quantity[EnergyUnit]])
    assert(Quantity(1, watt).isInstanceOf[Quantity[PowerUnit]])
    assert(Quantity(1, coulomb).isInstanceOf[Quantity[ElectricChargeUnit]])
    assert(Quantity(1, volt).isInstanceOf[Quantity[ElectricPotentialDifferenceUnit]])
    assert(Quantity(1, farad).isInstanceOf[Quantity[CapacitanceUnit]])
    assert(Quantity(1, ohm).isInstanceOf[Quantity[ElectricResistanceUnit]])
    assert(Quantity(1, siemens).isInstanceOf[Quantity[ElectricConductanceUnit]])
    assert(Quantity(1, weber).isInstanceOf[Quantity[MagneticFluxUnit]])
    assert(Quantity(1, tesla).isInstanceOf[Quantity[MagneticFluxDensityUnit]])
    assert(Quantity(1, henry).isInstanceOf[Quantity[InductanceUnit]])
  }

  test("operational instances") {
    val o = Quantity(1, one)
    val l = Quantity(1, metre)
    val m = Quantity(1, kilogram)
    val t = Quantity(1, second)
    val i = Quantity(1, ampere)
    assert((l + l).isInstanceOf[Quantity[LengthUnit]])
    assert((t - t).isInstanceOf[Quantity[TimeUnit]])
    assert((i * t).isInstanceOf[Quantity[ElectricChargeUnit]])
    assert((o / t).isInstanceOf[Quantity[FrequencyUnit]])
    assert((m * ((l / t) / t)).isInstanceOf[Quantity[ForceUnit]])
  }

  test("operational results") {
    assertResult(Quantity(90, kilogram))(Quantity(100, kilogram)-Quantity(10000, gram))
    assertResult(Quantity(900, hertz))(Quantity(400, hertz)+Quantity(500, hertz))
    val m = Quantity(10, kilogram)
    val l = Quantity(0.981, metre)
    val t = Quantity(1, second)
    val f = m * ((l / t) / t)
    assertResult(Quantity(9.81, kilogram * ((metre / second) / second)))(f)
    assertResult(Quantity(9.81, newton))(f)
  }

  test("scalar arithmetics") {
    val q = Quantity(1, meter)
    val r = Quantity(2, meter)
    assertResult(r)(q*2)
    assertResult(r)(2*q)
  }

  test("prefixes") {
    val qkg = Quantity(1, kilogram)
    val qg = Quantity(1000, gram)
    val qmg = Quantity(1000000, milli * gram)
    assertResult(qkg)(qg)
    assertResult(qkg)(qmg)
  }

  test("shortcuts") {
    assertResult(Quantity(1, metre))(1.m)
    assertResult(Quantity(2, metre))(1.5.m+0.5.m)
    assertResult(Quantity(1, gram))(1e-3.kg)
    assertResult(Quantity(10, metrePerSecond))(10.m/1.s)
    assertResult(Quantity(9.81, newton))(9.81.kg*((1.m/1.s)/1.s))
    assertResult(100.Pa)(100*1.N/(1.m*1.m))
    assertResult(2500.J)(1000.N*2.5.m)
    assertResult(100.W)(100*1.J/1.s)
    assertResult(100.W)(200.V*0.5.A)
    assertResult(200.V)(200.W/1.A)
  }

  test("equality") {
    val l1a = Quantity(1, metre)
    val l1b = Quantity(1, metre)
    val l2 = Quantity(2, metre)
    val m = Quantity(1, kilogram)
    val m2a1 = Quantity(0.01, kilogram)
    val m2a2 = Quantity(0.01, kilogram)
    val m2b = Quantity(0.1*0.1, kilogram)
    assert(l1a == l1b)
    assert(l1a != l2)
    assert(l1a != m)
    assert(l1a ~= l1b)
    assert(!(l1a ~= l2))
    assert(!(l1a ~= m))
    assert(m2a1 ~= m2a2)
    assert(m2a1 ~= m2b)
    assert(!(m2a1 == m2b))
 }

}
