/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.math.linalg.Vector
import de.h2b.scala.lib.phys.units.base._
import de.h2b.scala.lib.phys.units.derived._

@RunWith(classOf[JUnitRunner])
class SiDerivedUnitsTest extends FunSuite {

  test("instances") {
	  assert((meter * meter).isInstanceOf[AreaUnit])
	  assert((meter * meter * meter).isInstanceOf[VolumeUnit])
	  assert((kilogram / (meter * meter * meter)).isInstanceOf[MassDensityUnit])
	  assert((ampere / squareMeter).isInstanceOf[CurrentDensityUnit])
	  assert((ampere / meter).isInstanceOf[MagneticFieldStrengthUnit])
	  assert((mole / cubicMeter).isInstanceOf[AmountConcentrationUnit])
	  assert((candela / squareMeter).isInstanceOf[LuminanceUnit])
	  assert((one / second).isInstanceOf[FrequencyUnit])
	  assert((kilogram * ((metre / second) / second)).isInstanceOf[ForceUnit])
	  assert((newton / squareMeter).isInstanceOf[PressureUnit])
	  assert((newton * meter).isInstanceOf[EnergyUnit])
	  assert((joule / second).isInstanceOf[PowerUnit])
	  assert((ampere * second).isInstanceOf[ElectricChargeUnit])
	  assert((watt / ampere).isInstanceOf[ElectricPotentialDifferenceUnit])
	  assert((volt * ampere).isInstanceOf[PowerUnit])
	  assert((coulomb / volt).isInstanceOf[CapacitanceUnit])
	  assert((volt / ampere).isInstanceOf[ElectricResistanceUnit])
	  assert((ampere / volt).isInstanceOf[ElectricConductanceUnit])
	  assert((volt * second).isInstanceOf[MagneticFluxUnit])
	  assert((weber / squareMeter).isInstanceOf[MagneticFluxDensityUnit])
	  assert((weber / ampere).isInstanceOf[InductanceUnit])
  }

  test("properties frequency") {
    assertResult(Vector[Int]()-Unit.sVec)(hertz.siExponents)
    assertResult(1.0)(hertz.scale)
    assertResult("Hz")(hertz.symbol)
    assertResult("hertz")(hertz.name)
    val h = one / second
    assertResult(Vector[Int]()-Unit.sVec)(h.siExponents)
    assertResult(1.0)(h.scale)
    assertResult("1/s")(h.symbol)
    assertResult("1/s")(h.name)
  }

  test("properties force") {
    assertResult(Unit.kgVec+Unit.mVec-2*Unit.sVec)(newton.siExponents)
    assertResult(1.0)(newton.scale)
    assertResult("N")(newton.symbol)
    assertResult("newton")(newton.name)
    val f = kilogram * ((metre / second) / second)
    assertResult(Unit.kgVec+Unit.mVec-2*Unit.sVec)(f.siExponents)
    assertResult(1.0)(f.scale)
    assertResult("kg*m/s/s")(f.symbol)
    assertResult("kg*m/s/s")(f.name)
  }

  test("properties charge") {
    assertResult(Unit.AVec+Unit.sVec)(coulomb.siExponents)
    assertResult(1.0)(coulomb.scale)
    assertResult("C")(coulomb.symbol)
    assertResult("coulomb")(coulomb.name)
    val as = ampere * second
    assertResult(Unit.AVec+Unit.sVec)(as.siExponents)
    assertResult(1.0)(as.scale)
    assertResult("A*s")(as.symbol)
    assertResult("A*s")(as.name)
  }

  test("coherence") {
	  assert(kilogram.isSi)
	  assert(gram.isSi)
	  assert(kilogram.isCoherent)
	  assert(!gram.isCoherent)
	  assertResult(1)(kilogram.coherentScale)
	  assertResult(1e-3)(gram.coherentScale)
	  assertResult(Unit.nVec+Unit.kgVec)(kilogram.coherentExponents) //workaround: (1)@2 does not equal (0,0,1)@0 by current design
	  assertResult(Unit.nVec+Unit.kgVec)(gram.coherentExponents) //workaround: see above
  }

}
