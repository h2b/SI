object readme {

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
    import de.h2b.scala.lib.phys.units.Quantity
    import de.h2b.scala.lib.phys.units.Quantity._
    import de.h2b.scala.lib.phys.units.base._
    import de.h2b.scala.lib.phys.units.derived._

    val m = Quantity(10, kilogram)                //> m  : de.h2b.scala.lib.phys.units.Quantity[de.h2b.scala.lib.phys.units.base.M
                                                  //| assUnit] = 10.0 kg
    val l = Quantity(0.981, metre)                //> l  : de.h2b.scala.lib.phys.units.Quantity[de.h2b.scala.lib.phys.units.base.L
                                                  //| engthUnit] = 0.981 m
    val t = Quantity(1, second)                   //> t  : de.h2b.scala.lib.phys.units.Quantity[de.h2b.scala.lib.phys.units.base.T
                                                  //| imeUnit] = 1.0 s

    val m2 = m + Quantity(20, kilogram)           //> m2  : de.h2b.scala.lib.phys.units.Quantity[de.h2b.scala.lib.phys.units.base.
                                                  //| MassUnit] = 30.0 
    val m3 = 2 * Quantity(1, meter)               //> m3  : de.h2b.scala.lib.phys.units.Quantity[de.h2b.scala.lib.phys.units.base.
                                                  //| LengthUnit] = 2.0 m

    val f = m * ((l / t) / t)                     //> f  : de.h2b.scala.lib.phys.units.Quantity[de.h2b.scala.lib.phys.units.derive
                                                  //| d.ForceUnit] = 9.81 kg*m/s/s
    
    Quantity(10, metre) == 10.m                   //> res0: Boolean = true

}