/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units.base

import de.h2b.scala.lib.math.linalg.Vector
import de.h2b.scala.lib.phys.units.base._
import de.h2b.scala.lib.phys.units.derived._
import de.h2b.scala.lib.phys.units.Unit
import de.h2b.scala.lib.phys.units.Unit._

class ElectricCurrentUnit (symbol: String, name: String = "", scale: Double = 1.0) extends
    Unit[ElectricCurrentUnit](Unit.AVec, symbol, name, scale)

object ElectricCurrentUnit {
	implicit val divAre =
			drule { (u1: Unit[ElectricCurrentUnit], u2: Unit[AreaUnit]) ⇒ new CurrentDensityUnit(_, _, _) }
	implicit val divLen =
			drule { (u1: Unit[ElectricCurrentUnit], u2: Unit[LengthUnit]) ⇒ new MagneticFieldStrengthUnit(_, _, _) }
	implicit val mulTim =
			mrule { (u1: Unit[ElectricCurrentUnit], u2: Unit[TimeUnit]) ⇒ new ElectricChargeUnit(_, _, _) }
	implicit val divPot=
			drule { (u1: Unit[ElectricCurrentUnit], u2: Unit[ElectricPotentialDifferenceUnit]) ⇒ new ElectricConductanceUnit(_, _, _) }
}
