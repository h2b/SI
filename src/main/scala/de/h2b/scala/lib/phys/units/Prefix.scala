/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

/**
 * An SI prefix.
 *
 * @author h2b
 */
sealed abstract class Prefix private[units] (val value: Int, val symbol: String, val name: String) {

  def * [U <: Unit[U]] (u: Unit[U]): Unit[U] = u.prefixed(this)

}

case object deca extends Prefix(1, "da", "deca")
case object hecto extends Prefix(2, "h", "hecto")
case object kilo extends Prefix(3, "k", "kilo")
case object mega extends Prefix(6, "M", "mega")
case object giga extends Prefix(9, "G", "giga")
case object tera extends Prefix(12, "T", "tera")
case object peta extends Prefix(15, "P", "peta")
case object exa extends Prefix(18, "E", "exa")
case object zetta extends Prefix(21, "Z", "zetta")
case object yotta extends Prefix(24, "Y", "yotta")

case object deci extends Prefix(-1, "d", "deci")
case object centi extends Prefix(-2, "c", "centi")
case object milli extends Prefix(-3, "m", "milli")
case object micro extends Prefix(-6, "µ", "micro")
case object nano extends Prefix(-9, "n", "nano")
case object pico extends Prefix(-12, "p", "pico")
case object femto extends Prefix(-15, "f", "femto")
case object atto extends Prefix(-18, "a", "atto")
case object zepto extends Prefix(-21, "z", "zepto")
case object yocto extends Prefix(-24, "y", "yocto")
