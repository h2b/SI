/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units.base

import de.h2b.scala.lib.math.linalg.Vector
import de.h2b.scala.lib.phys.units.base._
import de.h2b.scala.lib.phys.units.derived._
import de.h2b.scala.lib.phys.units.Unit
import de.h2b.scala.lib.phys.units.Unit._

class MassUnit (symbol: String, name: String = "", scale: Double = 1.0) extends
    Unit[MassUnit](Unit.kgVec, symbol, name, scale)

object MassUnit {
	implicit val divVol =
			drule { (u1: Unit[MassUnit], u2: Unit[VolumeUnit]) ⇒ new MassDensityUnit(_, _, _) }
	implicit val mulAcc =
			mrule { (u1: Unit[MassUnit], u2: Unit[AccelerationUnit]) ⇒ new ForceUnit(_, _, _) }
}
