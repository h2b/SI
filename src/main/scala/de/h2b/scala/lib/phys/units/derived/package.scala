/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

package object derived {

  val squareMetre = new AreaUnit("m^2", "m^2")
  val squareMeter = squareMetre //for convenience

  val cubicMetre = new VolumeUnit("m^3", "m^3")
  val cubicMeter = cubicMetre //for convenience

  val metrePerSecond = new VelocityUnit("m/s", "m/s")

  val metrePerSecondSquared = new AccelerationUnit("m/s^2", "m/s^2")

  val kilogramPerCubicMetre = new MassDensityUnit("kg/m^3", "kg/m^3")

  val amperePerSquareMetre = new CurrentDensityUnit("A/m^2", "A/m^2")

  val amperePerMetre = new MagneticFieldStrengthUnit("A/m", "A/m")

  val molePerCubicMetre = new AmountConcentrationUnit("mol/m^3", "mol/m^3")

  val candelaPerSquareMetre = new LuminanceUnit("cd/m^2", "cd/m^2")

  val hertz = new FrequencyUnit("Hz", "hertz")

  val newton = new ForceUnit("N", "newton")

  val pascal = new PressureUnit("Pa", "pascal")

  val joule = new EnergyUnit("J", "joule")

  val watt = new PowerUnit("W", "watt")

  val coulomb = new ElectricChargeUnit("C", "coulomb")

  val volt = new ElectricPotentialDifferenceUnit("V", "volt")

  val farad = new CapacitanceUnit("F", "farad")

  val ohm = new ElectricResistanceUnit("Ω", "ohm")

  val siemens = new ElectricConductanceUnit("S", "siemens")

  val weber = new MagneticFluxUnit("Wb", "weber")

  val tesla = new MagneticFluxDensityUnit("T", "tesla")

  val henry = new InductanceUnit("H", "henry")

}
