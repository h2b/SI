/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

import de.h2b.scala.lib.math.linalg.Vector
import de.h2b.scala.lib.phys.units.base._
import de.h2b.scala.lib.phys.units.derived._

/**
 * A unit of measure.
 *
 * Decomposition of this unit [Q] into SI base units according to the equation:
 *
 * [Q] = ξ·10^n^·m^α^·kg^β^·s^γ^·A^δ^·K^ε^·mol^ζ^·cd^η^
 *
 * The exponents are represented by a vector starting with index 0 for `n`
 * etc while `ξ` becomes an own field `scale`.
 *
 * Two units are considered to be equal if their SI exponents and scale are
 * the same, respectively. The name and symbol strings have no meaning
 * regarding to equality.
 *
 * For `ξ==1` we have a (potentially derived) ''SI unit''.
 *
 * For `ξ==1` and `n==0` we have a ''coherent SI unit''.
 *
 * @author h2b
 *
 * @see [[https://de.wikipedia.org/wiki/Internationales_Einheitensystem]]
 *
 * @constructor
 *
 * Constructs a unit of measure [Q] with
 * [Q] = ξ·10^n^·m^α^·kg^β^·s^γ^·A^δ^·K^ε^·mol^ζ^·cd^η^.
 *
 * @param <U> the type of this unit
 * @param siExponents the vector of integer exponents according to [Q]
 * starting with index 0 for `n`
 * @param symbol this unit's symbol string (informal)
 * @param name this unit's name string (informal)
 * @param scale the ξ in [Q]
 */
class Unit [U <: Unit[_]] (val siExponents: Vector[Int],
    val symbol: String, val name: String = "", val scale: Double = 1.0) extends Equals {

  import Unit._

  /**
   * @return Is this a (potentially derived) ''SI unit''?
   */
  def isSi: Boolean = scale==1

  /**
   * @return Is this a ''coherent SI unit''?
   */
  def isCoherent: Boolean = isSi && siExponents(0)==0

  /**
   * ξ·10^n.
   */
  val coherentScale = scale * math.pow(10, siExponents(0))

  /**
   * Exponents of the associated coherent SI unit.
   */
  val coherentExponents = siExponents - Vector(siExponents(0))@@nIdx

  def * [V <: Unit[_], W <: Unit[_]] (other: Unit[V]) (implicit mul: Mul4[Unit[U], Unit[V], W]) : W = {
    val exp2 = this.siExponents+other.siExponents
    val scale2 = this.scale*other.scale
    val symbol2 = this.symbol+"*"+other.symbol
    mul(this, other)(exp2, symbol2, symbol2, scale2) ensuring(u ⇒ u.siExponents~~exp2 && u.scale==scale2)
  }

  def / [V <: Unit[_], W <: Unit[_]] (other: Unit[V]) (implicit div: Div4[Unit[U], Unit[V], W]) : W = {
    val exp2 = this.siExponents-other.siExponents
    val scale2 = this.scale/other.scale
    val symbol2 = this.symbol+"/"+other.symbol
    div(this, other)(exp2, symbol2, symbol2, scale2) ensuring(u ⇒ u.siExponents~~exp2 && u.scale==scale2)
  }

  /**
   * @param p
   * @return a new unit with this scale and the other fields cumulative adjusted to the
   * specified prefix
   */
  def prefixed (p: Prefix): Unit[U] =
    new Unit[U](siExponents+Vector(p.value)@@nIdx, p.symbol+symbol, p.name+name, scale)

  /**
   * @param s
   * @return a new unit equal to this except for the scale field which is cumulative set
   * to `s*this.scale`
   */
  def scaled (s: Double): Unit[U] = new Unit[U](siExponents, symbol, name, s*scale)

  /**
   * @param n
   * @return a new unit equal to this except for the name field which is set
   * to `n`
   */
  def named (n: String): Unit[U] = new Unit[U](siExponents, symbol, n, scale)

  /**
   * @param s
   * @return a new unit equal to this except for the symbol field which is set
   * to `s`
   */
  def symboled (s: String): Unit[U] = new Unit[U](siExponents, s, name, scale)

  override def toString = symbol

  def toDebug = {
    val builder = new StringBuilder
    builder ++= s"$name: "
    builder ++= s"Symbol=$symbol, "
    for (i ← 0 to 7 if idxNames(i)!=0) builder ++= s"${idxNames(i)}=${siExponents(i)}, "
    builder ++= s"scale=$scale\n"
    builder.result()
  }

  def canEqual(other: Any) = {
		  other.isInstanceOf[Unit[U]]
  }

  override def equals(other: Any) = {
	  other match {
		  case that: Unit[U] => that.canEqual(this) && siExponents == that.siExponents && scale == that.scale
		  case _ => false
	  }
  }

  override def hashCode() = {
	  val prime = 41
	  prime * (prime + siExponents.hashCode) + scale.hashCode
  }

}

object Unit {

  //[Q] = ξ·10^n^·m^α^·kg^β^·s^γ^·A^δ^·K^ε^·mol^ζ^·cd^η^

  private[units] final val nIdx = 0
  private[units] final val mIdx = 1
  private[units] final val kgIdx = 2
  private[units] final val sIdx = 3
  private[units] final val AIdx = 4
  private[units] final val KIdx = 5
  private[units] final val molIdx = 6
  private[units] final val cdIdx = 7

  private final val idxNames = Seq("n", "m", "kg", "s", "A", "K", "mol", "cd")

  private[units] final val nVec = Vector(0)@@nIdx
  private[units] final val mVec = Vector(1)@@mIdx
  private[units] final val kgVec = Vector(1)@@kgIdx
  private[units] final val sVec = Vector(1)@@sIdx
  private[units] final val AVec = Vector(1)@@AIdx
  private[units] final val KVec = Vector(1)@@KIdx
  private[units] final val molVec = Vector(1)@@molIdx
  private[units] final val cdVec = Vector(1)@@cdIdx

  trait MulFunc [+W] extends Function4[Vector[Int], String, String, Double, W]

  type Mul4 [-U1 <: Unit[_], -U2 <: Unit[_], +W <: Unit[_]] = (U1, U2) ⇒ MulFunc[W]

  type Mul3 [-U1 <: Unit[_], -U2 <: Unit[_], +W <: Unit[_]] = (U1, U2) ⇒ (String, String, Double) ⇒ W

  def mrule [U1 <: Unit[_], U2 <: Unit[_], W <: Unit[_]] (m3: ⇒ Mul3[U1, U2, W]): Mul4[U1, U2, W] =
    (u1, u2) ⇒ new MulFunc[W] {
		  def apply (v: Vector[Int], s1: String, s2: String, d: Double) = m3(u1, u2)(s1, s2, d)
    }

  implicit val mul0: Mul4[Unit[_], Unit[_], Unit[_]] =
    (u1, u2) ⇒ new MulFunc[Unit[_]] {
      def apply (v: Vector[Int], s1: String, s2: String, d: Double) = new Unit(v, s1, s2, d)
    }

  trait DivFunc [+W] extends Function4[Vector[Int], String, String, Double, W]

  type Div4 [-U1 <: Unit[_], -U2 <: Unit[_], +W <: Unit[_]] = (U1, U2) ⇒ DivFunc[W]

  type Div3 [-U1 <: Unit[_], -U2 <: Unit[_], +W <: Unit[_]] = (U1, U2) ⇒ (String, String, Double) ⇒ W

  def drule [U1 <: Unit[_], U2 <: Unit[_], W <: Unit[_]] (m3: ⇒ Div3[U1, U2, W]): Div4[U1, U2, W] =
    (u1, u2) ⇒ new DivFunc[W] {
		  def apply (v: Vector[Int], s1: String, s2: String, d: Double) = m3(u1, u2)(s1, s2, d)
    }

  implicit val div0: Div4[Unit[_], Unit[_], Unit[_]] =
    (u1, u2) ⇒ new DivFunc[Unit[_]] {
      def apply (v: Vector[Int], s1: String, s2: String, d: Double) = new Unit(v, s1, s2, d)
    }

}
