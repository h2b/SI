/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

package object base {

  val one = new NeutralUnit("1", "one")

  val metre = new LengthUnit("m", "metre")
  val meter = metre //for convenience

  val kilogram = new MassUnit("kg", "kilogram")
  val gram = kilogram.prefixed(milli).symboled("g").named("gram")

  val second = new TimeUnit("s", "second")

  val ampere = new ElectricCurrentUnit("A", "ampere")

  val kelvin = new ThermodynamicTemperatureUnit("K", "kelvin")

  val mole = new AmountOfSubstanceUnit("mol", "mole")

  val candela = new LuminousIntensityUnit("cd", "candela")

}
