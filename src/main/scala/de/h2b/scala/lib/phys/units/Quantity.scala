/*
  SI - A Scala Library of Units of Measurement

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys.units

import de.h2b.scala.lib.math._
import de.h2b.scala.lib.phys.units.base._
import de.h2b.scala.lib.phys.units.derived._

/**
 * A physical quantity composed of a magnitude and a unit.
 *
 * @author h2b
 */
class Quantity [U <: Unit[_]] private (val magnitude: Double, val unit: Unit[U]) extends Equals {

  private val normalizedMagnitude = magnitude * unit.coherentScale

  private val normalizedUnit = new Unit[U](unit.coherentExponents, "")

  def + [V >: U <: Unit[V]] (other: Quantity[V]): Quantity[V] =
    new Quantity[V](normalizedMagnitude+other.normalizedMagnitude, other.normalizedUnit)

  def - [V >: U <: Unit[V]] (other: Quantity[V]): Quantity[V] =
    new Quantity[V](normalizedMagnitude-other.normalizedMagnitude, other.normalizedUnit)

  def * [V <: Unit[_], W <: Unit[_]] (other: Quantity[V]) (implicit mul: Unit.Mul4[Unit[U], Unit[V], Unit[W]]) : Quantity[W] =
    new Quantity[W](magnitude*other.magnitude, this.unit*other.unit)

  def / [V <: Unit[_], W <: Unit[_]] (other: Quantity[V]) (implicit div: Unit.Div4[Unit[U], Unit[V], Unit[W]]) : Quantity[W] =
    new Quantity[W](magnitude/other.magnitude, this.unit/other.unit)

  def * (s: Double): Quantity[U] = new Quantity(s*magnitude, unit)

  override def toString = s"$magnitude $unit"

  def canEqual(other: Any) = {
    other.isInstanceOf[Quantity[U]]
  }

  override def equals (other: Any) = {
    other match {
      case that: Quantity[U] =>
        that.canEqual(this) && normalizedMagnitude == that.normalizedMagnitude &&
        normalizedUnit == that.normalizedUnit
      case _ =>
        false
    }
  }

  override def hashCode() = {
    val prime = 41
    prime * (prime + normalizedMagnitude.hashCode) + normalizedUnit.hashCode
  }

  /**
   * Compares quantities within tolerance.
   */
  def ~= (other: Quantity[_]) (implicit tolerance: Tolerance[Double]): Boolean =
    (normalizedMagnitude - other.normalizedMagnitude).abs < tolerance.epsilon  &&
        normalizedUnit == other.normalizedUnit

}

object Quantity {

	/**
	 * @param magnitude
	 * @param unit
	 * @return a new physical quantity composed of the given magnitude and unit
	 */
	def apply [U <: Unit[_]] (magnitude: Double, unit: Unit[U]): Quantity[U] = new Quantity(magnitude, unit)

  implicit class ScalarOps (x: Double) {

	  def * [U <: Unit[_]] (q: Quantity[U]): Quantity[U] = q*x

	  def m (): Quantity[LengthUnit] = Quantity(x, metre)
	  def kg (): Quantity[MassUnit] = Quantity(x, kilogram)
	  def s (): Quantity[TimeUnit] = Quantity(x, second)
	  def A (): Quantity[ElectricCurrentUnit] = Quantity(x, ampere)
	  def K (): Quantity[ThermodynamicTemperatureUnit] = Quantity(x, kelvin)
	  def mol (): Quantity[AmountOfSubstanceUnit] = Quantity(x, mole)
	  def cd (): Quantity[LuminousIntensityUnit] = Quantity(x, candela)

	  def Hz (): Quantity[FrequencyUnit] = Quantity(x, hertz)
	  def N (): Quantity[ForceUnit] = Quantity(x, newton)
	  def Pa (): Quantity[PressureUnit] = Quantity(x, pascal)
	  def J (): Quantity[EnergyUnit] = Quantity(x, joule)
	  def W (): Quantity[PowerUnit] = Quantity(x, watt)
	  def C (): Quantity[ElectricChargeUnit] = Quantity(x, coulomb)
	  def V (): Quantity[ElectricPotentialDifferenceUnit] = Quantity(x, volt)
	  def F (): Quantity[CapacitanceUnit] = Quantity(x, farad)
	  def Ω (): Quantity[ElectricResistanceUnit] = Quantity(x, ohm)
	  def S (): Quantity[ElectricConductanceUnit] = Quantity(x, siemens)
	  def Wb (): Quantity[MagneticFluxUnit] = Quantity(x, weber)
	  def T (): Quantity[MagneticFluxDensityUnit] = Quantity(x, tesla)
	  def H (): Quantity[InductanceUnit] = Quantity(x, henry)

	}

}
