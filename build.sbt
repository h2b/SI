name := "SI"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
	"de.h2b.scala.lib" %% "utilib" % "0.4.0",
	"de.h2b.scala.lib.math" %% "linalg" % "2.1.0",
	"org.scalatest" %% "scalatest" % "3.0.1" % "test",
	"junit" % "junit" % "4.12" % "test"
)
